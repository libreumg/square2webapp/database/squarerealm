package de.square.realm;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.Principal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.catalina.realm.RealmBase;
import org.jooq.CloseableDSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import de.jooqFaces.EJooqFacesApplicationScope;
import de.jooqFaces.javax.JavaxJooqFacesContext;
//import square2.db.control.DeploymentGateway;
//import square2.help.ContextKey;
//import square2.help.PropertyFileDefinition;

/**
 * 
 * @author henkej
 *
 */
public class LoginRealm extends RealmBase {

	@Override
	protected String getPassword(String username) {
		return null;
	}

	@Override
	protected Principal getPrincipal(String username) {
		return new Principal() {
			@Override
			public String getName() {
				// TODO: check if the user exists in V_LOGIN
				String propFileName = PropertyFileDefinition.SQUARE2_CONFIGFILE;
				Properties prop = new Properties();
				try {
					prop.load(new FileInputStream(propFileName));

					String url = (String) prop.get(EJooqFacesApplicationScope.JOOQ_FACES_URL.get());
					String dialect = (String) prop.get(EJooqFacesApplicationScope.JOOQ_FACES_DRIVER.get());

					Connection con = DriverManager.getConnection(url);
					CloseableDSLContext jooq = DSL.using(con, dialect);

//					return new DeploymentGateway(jooq).getUsernameIfExistsInDatabase(username, null);
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			}
		};
	}

	@Override
	public Principal authenticate(String username, String credentials) {
		return new Principal() {
			@Override
			public String getName() {
				// TODO: check if the user exists in V_LOGIN
				String propFileName = PropertyFileDefinition.SQUARE2_CONFIGFILE;
				Properties prop = new Properties();
				try {
					prop.load(new FileInputStream(propFileName));

					String url = (String) prop.get(EJooqFacesApplicationScope.JOOQ_FACES_URL.get());
					String dialect = (String) prop.get(EJooqFacesApplicationScope.JOOQ_FACES_DRIVER.get());

					Connection con = DriverManager.getConnection(url);
					CloseableDSLContext jooq = DSL.using(con, dialect);

					return new DeploymentGateway(jooq).getUsernameIfExistsInDatabase(username, credentials);
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
			}
		};
	}
}
